"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _vueRouter = _interopRequireDefault(require("vue-router"));

var _Main = _interopRequireDefault(require("../views/Main1.vue"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_vue["default"].use(_vueRouter["default"]);

var routes = [{
  path: '/',
  name: 'Main1',
  component: _Main["default"]
}];
var router = new _vueRouter["default"]({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: routes
});
var _default = router;
exports["default"] = _default;